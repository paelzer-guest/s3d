#! /bin/sh
set -e

if [ -z "$DIR" ]; then
	DIR=s3d
fi

# try to download source package
if [ "$1" != "snapshot" ]; then
	uscan --verbose --force-download
else
	TMP="`mktemp -t -d`"
	git clone --bare "git://git.code.sf.net/p/${DIR}/code" "${TMP}"
	REV="$(git --git-dir "${TMP}" describe --long master | sed -e 's/^v*//' -e 's/-/+/g')"
	LONGREV="$(git --git-dir "${TMP}" rev-parse master)"
	TARNAME="${DIR}_${REV}.orig.tar"
	echo "${LONGREV}"
	git --git-dir "${TMP}" archive --format=tar --prefix="${DIR}-${REV}/" master -o "${TARNAME}"
	gzip -n -f "${TARNAME}"
	rm -rf "${TMP}"
fi
